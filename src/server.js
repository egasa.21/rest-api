const express = require('express'),
    mongoose = require('mongoose'),
    app = express(),
    routes = require('./routes'),
    cors = require('cors');

    require('dotenv').config()

app.use(cors());
app.use(express.json());
app.use(routes);

mongoose.connect(process.env.DATABASE_URL, {useNewUrlParser: true, useUnifiedTopology: true});
const db = mongoose.connection;
db.on('error', (err)=> console.log(err));
db.once('open', ()=> console.log('Database Connected'));


app.listen(1234, ()=> console.log('Running in port 1234'));